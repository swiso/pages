<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8" />
<title>Digital Rights Management | Defective by Design</title>
<link rel="icon" type="image/png" href="/images/favicon.png">
<link rel="canonical" href="https://switching.software/articles/digital-rights-management/" />
<link rel="stylesheet" href="/css/spectre.min.css">
<link rel="stylesheet" href="/css/spectre-icons.min.css">
<link rel="stylesheet" href="/css/custom.css">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="description" content="" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta property="og:type" content="article" />
<meta property="og:description" content="Defective by Design" />
<meta property="og:title" content="Digital Rights Management" />
<meta property="og:site_name" content="switching.software" />
<meta property="og:url" content="https://switching.software/articles/digital-rights-management/" />

<body id="top" class="type-articles m-0 p-0 slug-digital-rights-management">
    <div id="container">
        
<header id="header" class="p-fixed bg-gray">
    <div class="container grid-md">
        <div class="navbar">
            <section class="navbar-section">
                <div class="navbar-brand mt-2">
                    <a href="/" id="logo" class="text-dark">
                        switching.software
                    </a>
                </div>
            </section>
            <section class="navbar-section">
                <div class="dropdown dropdown-right">
                    <a href="/about" class="btn btn-link pt-2">About</a>
                </div>
            </section>
        </div>
    </div>
</header>

        <main id="content" class="container grid-md">
            
<div id="hero" class="text-center">
    <h1 class="mb-0">Digital Rights Management</h1>
    <h2><strong>Defective by Design</strong></h2>
</div>
    <p>Digital Rights Management (DRM) is where something digital (such as an app, a video or an eBook) is artificially restricted or crippled in some way. You never actually own something with DRM, you’re just “licensing it” for your own use under the manufacturer’s terms (the End User Licensing Agreement or EULA).</p>
<p>For example, if you buy a book on Kindle, Amazon reserves the right to delete it from your device without warning in the future (which is <a href="https://www.nytimes.com/2009/07/18/technology/companies/18amazon.html">something they have actually done</a>). Amazon even deliberately <a href="https://www.wired.com/2012/10/amazons-remote-wipe-of-customers-kindle-highlights-perils-of-drm/">deleted someone’s entire library</a>, and refused to tell her why.</p>
<p>A less serious but more widely experienced example is the <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">loud unskippable video</a> which many DVDs forced you to watch, and which had to be watched in full <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">every</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">single</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">time</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">you</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">watched</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">the</a> <a href="https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4">disc</a>. Only honest people who bought the disc had to watch it, pirates didn’t have to.</p>
<figure class="text-center">
    <img
        class="img-responsive mx-auto"
        src="/articles/digital-rights-management/movie-intro-screen.svg" width="400px" alt="screenshot of piracy intro on movies">
        <figcaption>
            <small><em>You wouldn&#39;t skip an ad</em></small>
        </figcaption>
</figure>
<p>More recent DRM methods often require connecting your product online and letting it  “phone home” your personal data to the manufacturer. PC games site Steam does this with their app, and if you remove the app any games bought through Steam also stop working. It is unclear what would happen if Steam itself went out of business. There have already been examples of legally purchased <a href="https://web.archive.org/web/20180904104351/https://twinfinite.net/2018/06/metal-gear-rising-no-longer-playable-on-mac-due-to-drm-shutdown/">DRM games being permanently broken</a> when their distributor shut down.</p>
<p>There is no benefit whatsoever to the customer from DRM. In fact, it <a href="https://www.mic.com/articles/29213/simcity-drm-always-online-mode-results-in-disaster-for-gamers">often makes products much worse</a>. In theory DRM is supposed to reduce piracy, but in practice pirated copies appear and spread online very quickly whether or not DRM is used. No DRM method has ever prevented piracy.</p>
<p>DRM continues to be used partly to reassure nervous copyright holders that their products are not going to be pirated (which is a lie), partly to stop you moving to a rival company’s services and partly to make more money by spying on you when you use the product. It punishes honest customers without giving them any benefits in return.</p>
<figure class="text-center">
    <img
        class="img-responsive mx-auto"
        src="/articles/digital-rights-management/sad-book.svg" width="300px" alt="sad book with padlock">
        <figcaption>
            <small><em>CC-BY-SA based on DefectiveByDesign.org</em></small>
        </figcaption>
</figure>
<p>There is some awareness of how counterproductive DRM is. The games store <a href="https://www.gog.com/">GOG.com</a> only sells DRM-free games. Others have a more mixed approach, such as <a href="https://swiso.org/unofficial-kobo-search/">Kobo’s eBooks store</a> which sells both DRM and DRM-free titles. Furthermore, initiatives like <a href="https://www.defectivebydesign.org">Defective By Design</a> actively work against it.</p>
<p>If you can, try to buy DRM-free. It will give you a better product, more choice, more privacy, and it means you can actually buy stuff instead of just borrowing it.</p>


        </main>

        <footer id="footer" class="text-center bg-gray p-2">
    <hr class="d-none">
    <div class="container grid-md">
        <div class="columns">
            <div class="column col-md-12 col-7">
                <small>
                    <a title="Link to our License" href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener">CC-BY-SA 4.0</a>
                    by <a title="About this website" href="/about">switching.software</a>
                    <br class="show-xs"/>
                    + <a title="switching.social on web archive" href="https://web.archive.org/web/20190915101437/https://switching.social/" target="_blank" rel="noopener">switching.social</a>
                    + <a title="We say Thank You" href="/thanks" > &#10084; </a>
                </small>
            </div>
            <div class="column col-md-12 col-5">
                <small>
                    <a href="/faq" title="Frequently Asked Questions">FAQ</a> |
                    <a href="/support" title="How to support us">Support</a> |
                    <a href="https://codeberg.org/swiso/website?lang=en-US" title="Git repository on Codeberg.org" target="_blank" rel="noopener">Source</a> |
                    <a rel="me" href="https://fedifreu.de/@switchingsoftware" title="Follow us on Mastodon" target="_blank" rel="noopener">Mastodon</a>
                </small>
            </div>
        </div>
    </div>
</footer>

    </div>
</body>
</html>