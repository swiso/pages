<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Articles | switching.software</title>
    <link>https://switching.software/articles/</link>
    <description>Recent content in Articles on switching.software</description>
    <generator>Hugo -- gohugo.io</generator>
    <copyright>CC-BY-SA 4.0 by switching.software &amp; switching.social</copyright>
    <lastBuildDate>Mon, 07 Oct 2024 22:51:39 +0200</lastBuildDate>
    
	<atom:link href="https://switching.software/articles/index.xml" rel="self" type="application/rss+xml" />
    
    
    <item>
      <title>About this Site | switching.software</title>
      <link>https://switching.software/about/</link>
      <pubDate>Wed, 04 Sep 2024 17:35:10 +0200</pubDate>
      
      <guid>https://switching.software/about/</guid>
      <description>&lt;p&gt;&lt;strong&gt;switching.software&lt;/strong&gt; is a grassroots website, that is trying to let people know about ethical and easy-to-use alternatives to well-known websites, apps and other software.&lt;/p&gt;
&lt;h3 id=&#34;contacting-this-site&#34;&gt;Contacting this site&lt;/h3&gt;
&lt;p&gt;You can email this site via &lt;a href=&#34;https://switching.software/forms/contact/index.php&#34;&gt;our contact form&lt;/a&gt; or directly via this address:&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/about/contact.min.svg&#34;&gt;
&lt;/figure&gt;
&lt;h3 id=&#34;following-this-site&#34;&gt;Following this site&lt;/h3&gt;
&lt;p&gt;You can follow switching.software on the &lt;a href=&#34;https://switching.software/articles/federated-sites/&#34;&gt;Fediverse&lt;/a&gt; via &lt;a href=&#34;https://fedifreu.de/@switchingsoftware&#34;&gt;our Mastodon account&lt;/a&gt;. Other channels may follow in the future.&lt;/p&gt;
&lt;h3 id=&#34;origin-of-this-site&#34;&gt;Origin of this site&lt;/h3&gt;
&lt;p&gt;Switching.software is a fork of a former website known as &lt;a href=&#34;https://web.archive.org/web/20190915101437/https://switching.social/&#34;&gt;&amp;lsquo;switching.social&amp;rsquo;&lt;/a&gt;. It was created and maintained by an anonymous person and gained popularity in 2018 and 2019 - especially on the &lt;a href=&#34;https://switching.software/articles/federated-sites/&#34;&gt;Fediverse&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;In September 2019, the author took down all social media channels and the website itself. There hasn&amp;rsquo;t been any statement by them ever since. No matter the reasons, &lt;a href=&#34;https://switching.software/thanks/&#34;&gt;we want to thank them a lot&lt;/a&gt; for what they have achieved with this project - and wish them well.&lt;/p&gt;
&lt;p&gt;To keep the project alive, the site was restored from different sources - resulting in &lt;a href=&#34;https://codeberg.org/swiso-en/archive&#34;&gt;a bunch of HTML pages&lt;/a&gt;. Since then, a loose team of enthusiasts has started a fork based on the original articles. We want to maintain and improve the project. Progress on this endeavor can be seen in &lt;a href=&#34;https://codeberg.org/swiso/website&#34;&gt;our repository&lt;/a&gt;.&lt;/p&gt;
&lt;h3 id=&#34;copyright--licensing&#34;&gt;Copyright / Licensing&lt;/h3&gt;
&lt;p&gt;The content of this site and of &amp;lsquo;switching.social&amp;rsquo; is available under a &lt;a href=&#34;https://creativecommons.org/licenses/by-sa/4.0/&#34;&gt;Creative Commons Attribution Sharealike 4.0 Licence&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;If you use it, please remember to give proper credit to this site, &lt;a href=&#34;https://switching.software/&#34;&gt;switching.software&lt;/a&gt;, and link to the &lt;a href=&#34;https://creativecommons.org/licenses/by-sa/4.0/&#34;&gt;license&lt;/a&gt;. We, in turn, will always give credit to the original site &lt;a href=&#34;https://web.archive.org/web/20190915101437/https://switching.social/&#34;&gt;&amp;lsquo;switching.social&amp;rsquo;&lt;/a&gt;.&lt;/p&gt;
&lt;h3 id=&#34;privacy&#34;&gt;Privacy&lt;/h3&gt;
&lt;p&gt;This site collects no data on its visitors. There are no analytics, no cookies and no trackers involved.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Digital Rights Management | Defective by Design</title>
      <link>https://switching.software/articles/digital-rights-management/</link>
      <pubDate>Sat, 25 Apr 2020 17:51:05 +0200</pubDate>
      
      <guid>https://switching.software/articles/digital-rights-management/</guid>
      <description>&lt;p&gt;Digital Rights Management (DRM) is where something digital (such as an app, a video or an eBook) is artificially restricted or crippled in some way. You never actually own something with DRM, you’re just “licensing it” for your own use under the manufacturer’s terms (the End User Licensing Agreement or EULA).&lt;/p&gt;
&lt;p&gt;For example, if you buy a book on Kindle, Amazon reserves the right to delete it from your device without warning in the future (which is &lt;a href=&#34;https://www.nytimes.com/2009/07/18/technology/companies/18amazon.html&#34;&gt;something they have actually done&lt;/a&gt;). Amazon even deliberately &lt;a href=&#34;https://www.wired.com/2012/10/amazons-remote-wipe-of-customers-kindle-highlights-perils-of-drm/&#34;&gt;deleted someone’s entire library&lt;/a&gt;, and refused to tell her why.&lt;/p&gt;
&lt;p&gt;A less serious but more widely experienced example is the &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;loud unskippable video&lt;/a&gt; which many DVDs forced you to watch, and which had to be watched in full &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;every&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;single&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;time&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;you&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;watched&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;the&lt;/a&gt; &lt;a href=&#34;https://ia800204.us.archive.org/15/items/youtube-K_vHwfDNGdg/Piracy_It_s_A_Crime-K_vHwfDNGdg.mp4&#34;&gt;disc&lt;/a&gt;. Only honest people who bought the disc had to watch it, pirates didn’t have to.&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/articles/digital-rights-management/movie-intro-screen.svg&#34; width=&#34;400px&#34; alt=&#34;screenshot of piracy intro on movies&#34;&gt;
        &lt;figcaption&gt;
            &lt;small&gt;&lt;em&gt;You wouldn&amp;#39;t skip an ad&lt;/em&gt;&lt;/small&gt;
        &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;More recent DRM methods often require connecting your product online and letting it  “phone home” your personal data to the manufacturer. PC games site Steam does this with their app, and if you remove the app any games bought through Steam also stop working. It is unclear what would happen if Steam itself went out of business. There have already been examples of legally purchased &lt;a href=&#34;https://web.archive.org/web/20180904104351/https://twinfinite.net/2018/06/metal-gear-rising-no-longer-playable-on-mac-due-to-drm-shutdown/&#34;&gt;DRM games being permanently broken&lt;/a&gt; when their distributor shut down.&lt;/p&gt;
&lt;p&gt;There is no benefit whatsoever to the customer from DRM. In fact, it &lt;a href=&#34;https://www.mic.com/articles/29213/simcity-drm-always-online-mode-results-in-disaster-for-gamers&#34;&gt;often makes products much worse&lt;/a&gt;. In theory DRM is supposed to reduce piracy, but in practice pirated copies appear and spread online very quickly whether or not DRM is used. No DRM method has ever prevented piracy.&lt;/p&gt;
&lt;p&gt;DRM continues to be used partly to reassure nervous copyright holders that their products are not going to be pirated (which is a lie), partly to stop you moving to a rival company’s services and partly to make more money by spying on you when you use the product. It punishes honest customers without giving them any benefits in return.&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/articles/digital-rights-management/sad-book.svg&#34; width=&#34;300px&#34; alt=&#34;sad book with padlock&#34;&gt;
        &lt;figcaption&gt;
            &lt;small&gt;&lt;em&gt;CC-BY-SA based on DefectiveByDesign.org&lt;/em&gt;&lt;/small&gt;
        &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;There is some awareness of how counterproductive DRM is. The games store &lt;a href=&#34;https://www.gog.com/&#34;&gt;GOG.com&lt;/a&gt; only sells DRM-free games. Others have a more mixed approach, such as &lt;a href=&#34;https://swiso.org/unofficial-kobo-search/&#34;&gt;Kobo’s eBooks store&lt;/a&gt; which sells both DRM and DRM-free titles. Furthermore, initiatives like &lt;a href=&#34;https://www.defectivebydesign.org&#34;&gt;Defective By Design&lt;/a&gt; actively work against it.&lt;/p&gt;
&lt;p&gt;If you can, try to buy DRM-free. It will give you a better product, more choice, more privacy, and it means you can actually buy stuff instead of just borrowing it.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Free &amp; Open Software | More than free of charge</title>
      <link>https://switching.software/articles/free-libre-open-software/</link>
      <pubDate>Mon, 15 Jun 2020 11:11:28 +0900</pubDate>
      
      <guid>https://switching.software/articles/free-libre-open-software/</guid>
      <description>&lt;p&gt;Most pages and tools on switching.software are &lt;strong&gt;Free/Libre Software&lt;/strong&gt; or at least &lt;strong&gt;Open Source&lt;/strong&gt;. What is the difference? And what&amp;rsquo;s the benefit of it all? Let&amp;rsquo;s find out here.&lt;/p&gt;
&lt;h3 id=&#34;what-is-freelibre-software&#34;&gt;What is Free/Libre Software?&lt;/h3&gt;
&lt;p&gt;Free/Libre means, simply put, that everyone can &lt;strong&gt;use, study, distribute and adapt&lt;/strong&gt; a piece of software as they please. These four freedoms are the foundation of Free Software.&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;In this context, Free isn&amp;rsquo;t related to the price (&amp;ldquo;free of charge&amp;rdquo;), but to freedom. To avoid this ambiguity, the term Libre Software is often used.&lt;/p&gt;
&lt;p&gt;The majority won&amp;rsquo;t ever make full use of these four freedoms. But we all benefit from the possibilities they offer. They allow interested users to fix bugs, make improvements and share their changes with the world. Free Software goes hand in hand with &lt;strong&gt;Open Source&lt;/strong&gt; - the full disclosure of the source code.&lt;/p&gt;
&lt;h3 id=&#34;wheres-the-point-in-that&#34;&gt;Where&amp;rsquo;s the point in that?&lt;/h3&gt;
&lt;p&gt;In a nutshell: It creates more trust in software and more flexibility in its use. This can be illustrated in a little more detail with the following analogy.&lt;/p&gt;
&lt;p&gt;Suppose you want to offer a cake to your friends. Of course you can easily buy an off-the-shelf cake at the bakery. However, with this &lt;strong&gt;proprietary cake&lt;/strong&gt; it&amp;rsquo;s unclear what exactly it contains. Obviously, appearance and taste suggest some ingredients. But deducing &lt;em&gt;all&lt;/em&gt; the ingredients based on this is difficult or even impossible.&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/articles/free-libre-open-software/cake.png&#34; width=&#34;180px&#34;&gt;
        &lt;figcaption&gt;
            &lt;small&gt;&lt;em&gt;Proprietary Cake&lt;/em&gt;&lt;/small&gt;
        &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;This is a problem especially concerning allergies and food intolerances. More transparency - like an &lt;strong&gt;Open-Source-Cake&lt;/strong&gt; - helps you out. In fact, there are laws that require the labelling of allergens and often complete lists of ingredients. However, if your desired cake is no longer sold on celebration day, you still have a problem.&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/articles/free-libre-open-software/ingredients.png&#34; width=&#34;170px&#34;&gt;
        &lt;figcaption&gt;
            &lt;small&gt;&lt;em&gt;Open Cake&lt;/em&gt;&lt;/small&gt;
        &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;If the recipe is freely available, you can bake the cake yourself (or have it baked) at any time. You can follow all the steps and change individual ingredients as you like. And you can pass on the recipe to anyone interested. So you can enjoy the full flavour of &lt;strong&gt;Free/Libre Cake&lt;/strong&gt;.&lt;/p&gt;
&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/articles/free-libre-open-software/recipe.png&#34; width=&#34;170px&#34;&gt;
        &lt;figcaption&gt;
            &lt;small&gt;&lt;em&gt;Free/Libre Cake Recipe&lt;/em&gt;&lt;/small&gt;
        &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;Same story with software: Proprietary software also renders you dependent on one provider and may contain undesirable &amp;ldquo;ingredients&amp;rdquo;. Certain practices must likewise be labelled (e.g. in page-long &lt;a href=&#34;https://en.wikipedia.org/wiki/Terms_of_service&#34;&gt;Terms of Service&lt;/a&gt;), but this isn&amp;rsquo;t truly open and transparent.&lt;/p&gt;
&lt;p&gt;Free and Open Software, on the other hand, essentially exposes everything. So anyone can check its components, report errors (or fix them oneself), create their own variations and share them with others. All of this gives you more choice and more security - even as a pure &amp;ldquo;consumer&amp;rdquo;.&lt;/p&gt;
&lt;h3 id=&#34;open-vs-freelibre&#34;&gt;Open vs. Free/Libre&lt;/h3&gt;
&lt;p&gt;Some software companies publish the source code of their software, but do not grant anyone else the rights to modify or redistribute it. This causes some confusion around the term &amp;ldquo;open source&amp;rdquo;.&lt;/p&gt;
&lt;p&gt;Therefore, many in the community prefer to speak of FLOSS or FOSS, which explicitly combines the aspects of &lt;strong&gt;F&lt;/strong&gt;ree/&lt;strong&gt;L&lt;/strong&gt;ibre and &lt;strong&gt;O&lt;/strong&gt;pen-&lt;strong&gt;S&lt;/strong&gt;ource-&lt;strong&gt;S&lt;/strong&gt;oftware. For the sake of simplicity, on this website we will mainly use the term Libre.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Frequently Asked Questions | switching.software</title>
      <link>https://switching.software/articles/frequently-asked-questions/</link>
      <pubDate>Wed, 04 Sep 2024 17:35:10 +0200</pubDate>
      
      <guid>https://switching.software/articles/frequently-asked-questions/</guid>
      <description>&lt;h3 id=&#34;how-do-i-request--suggest-an-alternative&#34;&gt;How do I request / suggest an alternative?&lt;/h3&gt;
&lt;p&gt;You can contact our &lt;a href=&#34;https://fedifreu.de/@switchingsoftware&#34;&gt;Mastodon account&lt;/a&gt; or directly
&lt;a href=&#34;https://codeberg.org/swiso/website/issues&#34;&gt;open an issue&lt;/a&gt; on Codeberg.org.&lt;/p&gt;
&lt;h3 id=&#34;can-i-quote-switchingsoftwares-text-on-my-own-site&#34;&gt;Can I quote switching.software&amp;rsquo;s text on my own site?&lt;/h3&gt;
&lt;p&gt;Yes, the text on switching.software is available under a &lt;a href=&#34;https://creativecommons.org/licenses/by-sa/4.0/&#34;&gt;Creative
Commons Attribution Sharealike 4.0 Licence&lt;/a&gt;. Please remember to
link back to switching.software if you use content from this site, and
to include a link to the licence.&lt;/p&gt;
&lt;h3 id=&#34;can-i-translate-switchingsoftware-into-another-language&#34;&gt;Can I translate switching.software into another language?&lt;/h3&gt;
&lt;p&gt;Yes, absolutely! Please link back and add a link to the &lt;a href=&#34;https://creativecommons.org/licenses/by-sa/4.0/&#34;&gt;license&lt;/a&gt;
if you do so.&lt;/p&gt;
&lt;p&gt;There are already projects in French, German, Italian and
Turkish. They’re run by their translators, so please contact these
sites directly if you wish to contribute or suggest links.&lt;/p&gt;
&lt;h3 id=&#34;do-you-have-a-patreon-liberapay-or-other-way-to-financially-support-the-site&#34;&gt;Do you have a Patreon, Liberapay or other way to financially support the site?&lt;/h3&gt;
&lt;p&gt;Currently not, as switching.software&amp;rsquo;s site costs are quite
low. However, we recommend that you instead donate to free and open
source projects linked on the various pages. Pick the ones you
appreciate.&lt;/p&gt;
&lt;p&gt;If you want to &lt;a href=&#34;https://switching.software/support/&#34;&gt;support us non-financially&lt;/a&gt;, follow the
link.&lt;/p&gt;
&lt;h3 id=&#34;why-was-this-site-founded&#34;&gt;Why was this site founded?&lt;/h3&gt;
&lt;p&gt;As we haven&amp;rsquo;t founded this site ourselves, we want to quote from the
&lt;a href=&#34;https://web.archive.org/web/20190730203805/https://switching.social/about-this-site/&#34;&gt;original answer from &amp;lsquo;switching.social&amp;rsquo;&lt;/a&gt; here:&lt;/p&gt;
&lt;blockquote&gt;
&lt;p&gt;&lt;strong&gt;If a complete stranger knocked on your door&lt;/strong&gt; and asked for a copy
of all your emails in exchange for $1, would you accept such a deal?
Probably not, but if you use Gmail that’s the deal you’re accepting:
Google gets to read all your emails, and in exchange Google gives
you a mailbox that costs about $1 a month to run. [&amp;hellip;]&lt;/p&gt;
&lt;/blockquote&gt;
&lt;blockquote&gt;
&lt;p&gt;&lt;strong&gt;Many companies are now hopelessly addicted to gathering data.&lt;/strong&gt;
Their share price depends on not just gathering data but doing so at
an ever-greater pace, even driving them to break the law. [&amp;hellip;]&lt;/p&gt;
&lt;/blockquote&gt;
&lt;blockquote&gt;
&lt;p&gt;&lt;strong&gt;What can an individual do to stop all this?&lt;/strong&gt; How can one person
make a difference? By using apps and services that do not steal
data, and encouraging others to do the same. That’s what
switching.social is for: To make it as easy as possible for people
to find ethical alternatives that respect privacy.&lt;/p&gt;
&lt;/blockquote&gt;
&lt;h3 id=&#34;how-do-you-define-privacy-conscious-and-ethical&#34;&gt;How do you define &amp;ldquo;privacy-conscious&amp;rdquo; and &amp;ldquo;ethical&amp;rdquo;?&lt;/h3&gt;
&lt;p&gt;The idea of this site is to provide alternatives that have better
privacy but are as easy to use as whatever they replace. These are
referred to on this site as “privacy-conscious”.&lt;/p&gt;
&lt;p&gt;There are usually other alternatives that provide a higher level of
privacy but require technical knowledge that most people don’t
have. These aren’t listed in the main part of the site, though you may
find some of them in the &lt;a href=&#34;https://switching.software/list/advanced-users/&#34;&gt;Advanced Users&lt;/a&gt; and &lt;a href=&#34;https://switching.software/list/bubbling-under/&#34;&gt;Bubbling
Under&lt;/a&gt; sections.&lt;/p&gt;
&lt;p&gt;The word “ethical” is used in many different ways in modern
English. On this site it’s used to mean services and apps that do
significantly less harm to people’s privacy, because this site regards
user privacy as &lt;a href=&#34;https://en.wiktionary.org/wiki/ethical#Adjective&#34;&gt;morally good&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;For more details on this, you can also visit our &lt;a href=&#34;https://codeberg.org/swiso/website/src/branch/main/CONTRIBUTING.md#user-content-criteria-for-software&#34;&gt;list of criteria for
software&lt;/a&gt; aimed at contributors. In the end, none
of the alternatives listed on our website are perfect, but they are
better options.&lt;/p&gt;
&lt;h3 id=&#34;wont-these-alternatives-just-end-up-being-bought-by-big-companies&#34;&gt;Won&amp;rsquo;t these alternatives just end up being bought by big companies?&lt;/h3&gt;
&lt;p&gt;In most cases, no, that cannot happen.&lt;/p&gt;
&lt;p&gt;Most of the alternatives listed on here are technologically and
legally structured in a way that means they cannot be bought or sold.&lt;/p&gt;
&lt;p&gt;The social networks listed are &lt;a href=&#34;https://switching.software/articles/federated-sites/&#34;&gt;federated&lt;/a&gt;, which means
there is no single site to buy, just thousands of independent sites
linked together. Even if a particular site is sold, its users can move
to another independent site on the same network. It would be virtually
impossible for anyone to take control of the network as a whole.&lt;/p&gt;
&lt;p&gt;On top of all this, the software used is released under perpetual
&lt;a href=&#34;https://switching.software/articles/free-libre-open-software/&#34;&gt;free open licences&lt;/a&gt; that make it legally impossible for it to
be purchased.&lt;/p&gt;
&lt;h3 id=&#34;why-isnt-mapsme-listed-in-the-google-maps-alternatives&#34;&gt;Why isn&amp;rsquo;t Maps.me listed in the Google Maps alternatives?&lt;/h3&gt;
&lt;p&gt;The Maps.me site was bought by the Russian tech giant Mail.Ru
in 2014. Nowadays it promotes an app containing privacy-invading
trackers and advertisements.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>How to Switch | A List of Hints</title>
      <link>https://switching.software/articles/howto-switch/</link>
      <pubDate>Sun, 22 Dec 2019 21:15:38 +0100</pubDate>
      
      <guid>https://switching.software/articles/howto-switch/</guid>
      <description>&lt;p&gt;Here are some things to remember when you’re switching to privacy-friendly alternatives.&lt;/p&gt;
&lt;h3 id=&#34;dont-be-afraid&#34;&gt;Don&amp;rsquo;t be afraid&lt;/h3&gt;
&lt;p&gt;You don’t need to be a computer nerd. Lots of alternatives are easy to use, and the aim of switching.software is to only list these kinds of alternatives.&lt;/p&gt;
&lt;h3 id=&#34;start-small&#34;&gt;Start small&lt;/h3&gt;
&lt;p&gt;The idea of switching is to permanently move to more privacy-friendly services, and that can take a bit of time to do correctly. It’s better to switch in a sustainable way than to have a bad experience switching.&lt;/p&gt;
&lt;p&gt;Many people have a transition period where they use both old and new services simultaneously, or try out different alternatives before settling on one of them.&lt;/p&gt;
&lt;h3 id=&#34;tell-your-friends&#34;&gt;Tell your friends&lt;/h3&gt;
&lt;p&gt;The more visibility alternatives have, the more likely people are to try them out. You don’t need to preach, but simply saying that you’re using a particular service helps the service get onto people’s radar.&lt;/p&gt;
&lt;h3 id=&#34;dont-overextend-yourself&#34;&gt;Don&amp;rsquo;t overextend yourself&lt;/h3&gt;
&lt;p&gt;Not for every use case you might find an alternatives that suits your needs. It’s okay to just switch what you can.&lt;/p&gt;
&lt;p&gt;New alternatives are being developed all the time, and if there’s nothing that suits you right now there may be something suitable already in the pipeline. Take a look at &lt;a href=&#34;https://switching.software/list/bubbling-under/&#34;&gt;Bubbling Under&lt;/a&gt; for some examples.&lt;/p&gt;
&lt;h3 id=&#34;ask-for-help&#34;&gt;Ask for help&lt;/h3&gt;
&lt;p&gt;It&amp;rsquo;s fine if you don&amp;rsquo;t find a solution on your own. In case you don’t know anyone who can offer advice on alternatives, you can &lt;a href=&#34;https://switching.software/about/&#34;&gt;contact this site&lt;/a&gt;.&lt;/p&gt;
&lt;h3 id=&#34;help-others&#34;&gt;Help others&lt;/h3&gt;
&lt;p&gt;If you want to protect your own privacy, it&amp;rsquo;s a good idea to protect the privacy of other people as well. Because your personal data can also be found on your friends&amp;rsquo; devices. For example, think about your entry in their address book or your group selfies. Helping your friends to switch software is good for their privacy, and your own.&lt;/p&gt;
&lt;p&gt;The more people switch to alternatives, the better those alternatives become. The more people use alternative social networks, the more those networks become attractive to new members, and at some point they can snowball into the default choice. (This is the case with email for example, which is the default way of messaging on the internet despite no one promoting or owning the email network.)&lt;/p&gt;
&lt;p&gt;If someone asks for help getting away from Facebook, Google or wherever, do what you can to help them. Send them a link to switching.software and/or other sites like it.&lt;/p&gt;
&lt;h3 id=&#34;dont-fuel-unwanted-fears&#34;&gt;Don&amp;rsquo;t fuel unwanted fears&lt;/h3&gt;
&lt;p&gt;Some websites that discuss internet privacy are quite frightening. In a way they have good reason to be, because some dangerous things really are happening.&lt;/p&gt;
&lt;p&gt;However, not everyone wants to hear about the threat of mass surveillance when they’re eating their cornflakes or while already being concerned about their job / family / bills.&lt;/p&gt;
&lt;p&gt;A positive, non-scary approach may sometimes be a better way of letting people know about alternatives. For example, many people have observed that Mastodon is a lot more friendly than Twitter or Facebook.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Making Friends on the Fediverse | Getting Started on Alternative Social Media</title>
      <link>https://switching.software/articles/howto-make-friends-on-fediverse/</link>
      <pubDate>Sat, 04 Jan 2020 18:30:37 +0100</pubDate>
      
      <guid>https://switching.software/articles/howto-make-friends-on-fediverse/</guid>
      <description>&lt;p&gt;When you first sign up to a new social media site such as Diaspora, Friendica or Mastodon, it can feel a bit lonely. There are lots of ways you can make new friends and see more posts to read and comment on:&lt;/p&gt;
&lt;h3 id=&#34;introduce-yourself&#34;&gt;Introduce yourself&lt;/h3&gt;
&lt;p&gt;When you first sign up, post a little bit about yourself along with the hashtags &lt;strong&gt;#NewHere&lt;/strong&gt; and &lt;strong&gt;#Introductions&lt;/strong&gt;. Many existing users are on the lookout for these hashtags and will welcome you, perhaps offering to answer any questions you have. Make sure the visibility of this post is set to “public”, otherwise no one will see it.&lt;/p&gt;
&lt;h3 id=&#34;use-hashtags&#34;&gt;Use hashtags&lt;/h3&gt;
&lt;p&gt;The best way to attract attention on new social media is to add a &lt;strong&gt;#&lt;/strong&gt; sign in front of whatever topic you want to discuss in the post. For example, if you want to talk about basketball include &lt;em&gt;#basketball&lt;/em&gt; somewhere in your post. Some people put hashtags in the middle of what they are saying, others put them in a separate list at the end, but it doesn’t really matter as long as they’re included somewhere. The more relevant hashtags you have in your post, the more likely someone is to see your post and respond.&lt;/p&gt;
&lt;h3 id=&#34;follow-people&#34;&gt;Follow people&lt;/h3&gt;
&lt;p&gt;Don’t be shy about following others. On federated social media, your posts become more visible if you follow more people. Also remember to set your post’s visibility to “public” if you want it to reach the widest audience.&lt;/p&gt;
&lt;h3 id=&#34;follow-hashtags&#34;&gt;Follow hashtags&lt;/h3&gt;
&lt;p&gt;If you search for a hashtag (for example &lt;em&gt;#basketball&lt;/em&gt;) many sites will then give you the option of following or saving this hashtag. Every time someone includes a followed/saved hashtag in their posts, it will appear in your stream. On Friendica this option is a + sign just below the search box, and on Diaspora it’s a “follow #basketball” button.&lt;/p&gt;
&lt;h3 id=&#34;browse-the-public-stream&#34;&gt;Browse the Public Stream&lt;/h3&gt;
&lt;p&gt;Most alternative social media include an option to see an unfiltered random selection of the latest public posts from anyone on the service. You can find this under “Local Timeline”/”Federated Timeline” on Mastodon, “Local Community”/”Global Community” on Friendica and “Public Activity” on Diaspora. Bear in mind these will be totally unfiltered, so be careful, but you can sometimes find very interesting posts this way.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Support this Site | switching.software</title>
      <link>https://switching.software/support/</link>
      <pubDate>Wed, 04 Sep 2024 17:35:10 +0200</pubDate>
      
      <guid>https://switching.software/support/</guid>
      <description>&lt;p&gt;If you want to support this site, here are some things you can do.&lt;/p&gt;
&lt;h3 id=&#34;spread-the-word&#34;&gt;Spread the word&lt;/h3&gt;
&lt;p&gt;Recommend this website and &lt;a href=&#34;https://switching.software/replace/switching.software/&#34;&gt;its alternatives&lt;/a&gt; to friends that might be interested.&lt;/p&gt;
&lt;h3 id=&#34;proofread-our-site&#34;&gt;Proofread our site&lt;/h3&gt;
&lt;p&gt;This site is quite large and we are only a few persons working on it in our spare time. So, whenever you see outdated information, non-working links, typos or similar things, please let us know via our &lt;a href=&#34;https://fedifreu.de/@switchingsoftware&#34;&gt;Mastodon account&lt;/a&gt; or &lt;a href=&#34;https://codeberg.org/swiso/website/issues&#34;&gt;issue tracker&lt;/a&gt;.&lt;/p&gt;
&lt;h3 id=&#34;support-the-listed-projects&#34;&gt;Support the listed projects&lt;/h3&gt;
&lt;p&gt;Our site is only as good as the alternatives it has to offer. If you think a project is on the right track, give them your support. This doesn&amp;rsquo;t have to be financially. Often a helping hand on documentation, translations, discussions, artwork etc. is even more helpful. Something as simple as a motivating personal e-mail to the author may help a lot.&lt;/p&gt;
&lt;h3 id=&#34;contribute-on-codebergorg&#34;&gt;Contribute on Codeberg.org&lt;/h3&gt;
&lt;p&gt;If you have (or create) an account on &lt;a href=&#34;https://codeberg.org/&#34;&gt;Codeberg&lt;/a&gt;, check out our &lt;a href=&#34;https://codeberg.org/swiso/website&#34;&gt;website repository&lt;/a&gt; and the &lt;a href=&#34;https://codeberg.org/swiso/website/issues&#34;&gt;open issues&lt;/a&gt;. Feel free to &lt;a href=&#34;https://codeberg.org/swiso/website/src/branch/develop/CONTRIBUTING.md&#34;&gt;participate&lt;/a&gt; in discussions and open new issues for errors, outdated information or proposals concerning this website.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Thanks | switching.software</title>
      <link>https://switching.software/thanks/</link>
      <pubDate>Thu, 19 Mar 2020 20:20:16 +0100</pubDate>
      
      <guid>https://switching.software/thanks/</guid>
      <description>&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;&lt;strong&gt;We say thanks&lt;/strong&gt;&lt;br&gt;
to each and every one&lt;br&gt;
who participated in&lt;br&gt;
creating this.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Be it directly&lt;/strong&gt;&lt;br&gt;
via opening issues,&lt;br&gt;
discussing of content,&lt;br&gt;
drafting of texts and&lt;br&gt;
writing of code.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Be it indirectly&lt;/strong&gt;&lt;br&gt;
via asking questions,&lt;br&gt;
searching for answers,&lt;br&gt;
sharing the results and&lt;br&gt;
spreading the word.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Be it essentially&lt;/strong&gt;&lt;br&gt;
by contributing to any&lt;br&gt;
free / libre / open projects&lt;br&gt;
of any shape, size or form&lt;br&gt;
for a public benefit.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;This is yours.&lt;/strong&gt;&lt;br&gt;
A tribute to a space of&lt;br&gt;
hope, trust, and freedom&lt;br&gt;
with all its imperfections&lt;br&gt;
and inspirations.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;We say thanks&lt;/strong&gt;&lt;br&gt;
to you, and especially&lt;br&gt;
to the anonymous author&lt;br&gt;
who made this small website&lt;br&gt;
a part of the digital journey&lt;br&gt;
we keep on enjoying&lt;br&gt;
&lt;strong&gt;Together.&lt;/strong&gt;&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;&lt;a href=&#34;https://web.archive.org/web/20190915101437/https://switching.social/&#34;&gt;&lt;figure class=&#34;text-center&#34;&gt;
    &lt;img
        class=&#34;img-responsive mx-auto&#34;
        src=&#34;https://switching.software/thanks/switching-social.jpg&#34; alt=&#34;edited screenshot of switching.social&#34;&gt;
&lt;/figure&gt;&lt;/a&gt;&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>The Fediverse | What are Federated Sites?</title>
      <link>https://switching.software/articles/federated-sites/</link>
      <pubDate>Wed, 04 Sep 2024 17:35:10 +0200</pubDate>
      
      <guid>https://switching.software/articles/federated-sites/</guid>
      <description>&lt;p&gt;Most alternative social media are federated, decentralised or &amp;ldquo;part of the Fediverse&amp;rdquo;. But what does this actually mean?&lt;/p&gt;
&lt;p&gt;In a nutshell: Federated sites are &lt;strong&gt;split across many smaller sites&lt;/strong&gt; that are linked together. People who sign up on one small site can make friends with people on other small sites, because they’re part of the same federated network. From the user’s point of view, it’s just like using one large site. The federated social network Mastodon has quite a good &lt;a href=&#34;https://peertube.social/videos/watch/d7fabc85-f110-4699-beb0-7edf6d4082ba&#34;&gt;video explaining how decentralisation works&lt;/a&gt;.&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;This may sound complicated, but &lt;strong&gt;all of us use a federated system every day&lt;/strong&gt;. Whether it&amp;rsquo;s your  our cellphone contract, your bank account or your email address: It&amp;rsquo;s your choice, with which provider you settle. Based on a uniform identifier (IBAN, mobile phone number or email address), you can interact with everyone in the corresponding network. These options remain available to you even after changing the provider. All you have to do is pass on your new identifier to your contacts.&lt;/p&gt;
&lt;p&gt;Federated sites do the same for social networks. In general, the &lt;strong&gt;identifier of an account&lt;/strong&gt; is similar to an email address, prefixed with an additional &amp;lsquo;@&amp;rsquo; sign. It consists of your login name and the website you chose. An example of this is our &lt;a href=&#34;https://fedifreu.de/@switchingsoftware&#34;&gt;@switchingsoftware@fedifreu.de&lt;/a&gt; identifier.&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;The &amp;ldquo;providers&amp;rdquo; of the Fediverse are &lt;strong&gt;mainly private individuals and non-profit organizations&lt;/strong&gt;. They do not pursue commercial interests by operating their instances. In addition, none of them has control over the entire network. This results in a high level of protection against failures and censorship. In addition, this strengthens privacy, as there is no central database for evaluations.&lt;/p&gt;
&lt;p&gt;Most instances are comparatively &lt;strong&gt;small and manageable&lt;/strong&gt;. This makes it much easier to get in touch with administrators and moderators. The best-case scenario being: You know them personally (or are actually a part of them) and therefore have high confidence in them. Advanced users can also run their own instance - either on their own or supported by services like &lt;a href=&#34;https://masto.host&#34;&gt;masto.host&lt;/a&gt; or &lt;a href=&#34;https://app.spacebear.ee/&#34;&gt;app.spacebear.ee&lt;/a&gt;.&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;Despite all these advantages, the Fediverse is essentially a social network. It aims at the &lt;strong&gt;free and open exchange of information&lt;/strong&gt; and uses standardized protocols developed for this purpose (e.g. &lt;a href=&#34;https://activitypub.rocks/&#34;&gt;ActivityPub&lt;/a&gt; and &lt;a href=&#34;https://en.wikipedia.org/wiki/OStatus&#34;&gt;OStatus&lt;/a&gt;). End-to-end encryption is currently not part of these protocols. This means: Your messages are transmitted encrypted (transport encrypted), but admins of the participating instances still have access to the plain text.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;For maximum privacy&lt;/strong&gt;, you should therefore prefer services with end-to-end encryption. Otherwise, keep in mind that direct messages could also be read by admins of the involved instances - for example, if the recipient reports them. Furthermore, as with other services, the same applies in the Fediverse: Public messages are always visible to everyone. Accordingly, third parties can index and analyze them.&lt;/p&gt;
&lt;!-- raw HTML omitted --&gt;
&lt;p&gt;Nevertheless the Fediverse is more than worth a try. There are &lt;a href=&#34;https://the-federation.info/&#34;&gt;a lot of people&lt;/a&gt; who already use it - a &lt;a href=&#34;https://switching.software/list/fediverse/&#34;&gt;list of Fediverse services&lt;/a&gt; can be seen here. We also have some tipps on &lt;a href=&#34;https://switching.software/articles/howto-make-friends-on-fediverse/&#34;&gt;how to find friends in the Fediverse&lt;/a&gt;.&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>