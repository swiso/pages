<?php
    /* Load HazzelForm library */
    $hugo_path = 'forms/contact/';
    $hazzel_path = substr(__DIR__, 0, strlen(__DIR__) - strlen($hugo_path)) . "/submodules/HazzelForms/src/HazzelForms/HazzelForm.php";
    require_once $hazzel_path;

    /* Init form and honeypot fields */
        $form = new HazzelForms\HazzelForm(array(
            'autocomplete' => false,
            'lang' => 'EN'
        ));
        $honeypots = ['Fullname', 'Phone', 'Mail', 'Subject', 'Website'];
        $counter = 1;
        $reply = "";

    /* Create form fields */
            $form->addField('Name', 'text', [
                'label'       => 'Name',
                'placeholder' => 'Your Name',
                'classlist'   => 'form-input' ]);
            $form->addField($honeypots[$counter++], 'honeypot', [
                'classlist'   => 'd-none',
                'inline-css'  => false ]);
            $form->addField('Email', 'email', [
                'label'       => 'Email',
                'placeholder' => 'Your E-Mail',
                'classlist'   => 'form-input' ]);
            $form->addField($honeypots[$counter++], 'honeypot', [
                'classlist'   => 'd-none',
                'inline-css'  => false ]);
                $reply = 'Email';
            $form->addField('Message', 'textarea', [
                'label'       => 'Message',
                'placeholder' => 'Your Message',
                    'rows'    => '10',
                'classlist'   => 'form-input' ]);
            $form->addField($honeypots[$counter++], 'honeypot', [
                'classlist'   => 'd-none',
                'inline-css'  => false ]);

    /* Validate and send form */
        if ($form->validate()) {
            $to = getenv('SWISO_CONTACT_EMAIL');
            $from = getenv('SWISO_SENDER_EMAIL');
            if ($reply == "") {
                $reply = $to;
            } else {
                $reply = $form->getField($reply)->getValue();
            }

            $form->sendMail($to, $from, $reply);
        }
?>


<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8" />
<title>Contact | switching.software</title>
<link rel="icon" type="image/png" href="/images/favicon.png">
<link rel="canonical" href="https://switching.software/forms/contact/index.php" />
<link rel="stylesheet" href="/css/spectre.min.css">
<link rel="stylesheet" href="/css/spectre-icons.min.css">
<link rel="stylesheet" href="/css/custom.css">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="description" content="" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta property="og:type" content="article" />
<meta property="og:description" content="" />
<meta property="og:title" content="Contact" />
<meta property="og:site_name" content="switching.software" />
<meta property="og:url" content="https://switching.software/forms/contact/index.php" />

<body id="top" class="type-forms m-0 p-0 slug-contact">
    <div id="container">
        
<header id="header" class="p-fixed bg-gray">
    <div class="container grid-md">
        <div class="navbar">
            <section class="navbar-section">
                <div class="navbar-brand mt-2">
                    <a href="/" id="logo" class="text-dark">
                        switching.software
                    </a>
                </div>
            </section>
            <section class="navbar-section">
                <div class="dropdown dropdown-right">
                    <a href="/about" class="btn btn-link pt-2">About</a>
                </div>
            </section>
        </div>
    </div>
</header>

        <main id="content" class="container grid-md">
            
<div id="hero" class="text-center">
    <h1 class="mb-0">Contact</h1>
</div>
    <?php
    /* Build form on buffer */
        ob_start();
        $form->openForm();
        $counter = 1;
            $form->renderField('Name');
            $form->renderField($honeypots[$counter++]);
            $form->renderField('Email');
            $form->renderField($honeypots[$counter++]);
            $form->renderField('Message');
            $form->renderField($honeypots[$counter++]);
        $form->renderSubmitErrors();
        $form->renderSubmit('Send');

    /* Replace CSS classes with Spectre.css classes */
        $content = ob_get_clean();
        $content = str_replace( 'field-wrap', 'form-group', $content );
        $content = str_replace( 'label', 'form-label', $content );
        $content = str_replace( 'type="submit"', 'class="btn btn-primary p-centered" type="submit"', $content );
        echo $content;

    /* Prepare success message */
    ?>
    <div class="empty success-message">
        <div class="empty-icon">
            <i class="icon icon-3x icon-mail"></i>
        </div>
        <p class="empty-title h5">Your message was sent successfully.</p>
        <div class="empty-action">
            <a href="/" class="btn btn-primary">Go to homepage</a>
        </div>
    </div>

    <?php
        $form->closeForm();
    ?>

        </main>

        <footer id="footer" class="text-center bg-gray p-2">
    <hr class="d-none">
    <div class="container grid-md">
        <div class="columns">
            <div class="column col-md-12 col-7">
                <small>
                    <a title="Link to our License" href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener">CC-BY-SA 4.0</a>
                    by <a title="About this website" href="/about">switching.software</a>
                    <br class="show-xs"/>
                    + <a title="switching.social on web archive" href="https://web.archive.org/web/20190915101437/https://switching.social/" target="_blank" rel="noopener">switching.social</a>
                    + <a title="We say Thank You" href="/thanks" > &#10084; </a>
                </small>
            </div>
            <div class="column col-md-12 col-5">
                <small>
                    <a href="/faq" title="Frequently Asked Questions">FAQ</a> |
                    <a href="/support" title="How to support us">Support</a> |
                    <a href="https://codeberg.org/swiso/website?lang=en-US" title="Git repository on Codeberg.org" target="_blank" rel="noopener">Source</a> |
                    <a rel="me" href="https://fedifreu.de/@switchingsoftware" title="Follow us on Mastodon" target="_blank" rel="noopener">Mastodon</a>
                </small>
            </div>
        </div>
    </div>
</footer>

    </div>
</body>
</html>