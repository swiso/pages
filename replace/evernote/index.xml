<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Bye, Evernote | Recommended Note Taking:</title>
    <link>https://switching.software/replace/evernote/</link>
    <description>Recent content in Evernote on switching.software</description>
    <generator>Hugo -- gohugo.io</generator>
    <copyright>CC-BY-SA 4.0 by switching.software &amp; switching.social</copyright>
    <lastBuildDate>Wed, 24 Jul 2024 12:49:23 +0200</lastBuildDate>
    
	<atom:link href="https://switching.software/replace/evernote/index.xml" rel="self" type="application/rss+xml" />
    
    
    <item>
      <title>CryptPad | switching.software</title>
      <link>https://switching.software/use/cryptpad/</link>
      <pubDate>Fri, 12 Jul 2024 13:29:17 +0200</pubDate>
      
      <guid>https://switching.software/use/cryptpad/</guid>
      <description>&lt;p&gt;&lt;strong&gt;CryptPad&lt;/strong&gt; is a collection of &lt;a href=&#34;https://switching.software/articles/free-libre-open-software/&#34;&gt;open source&lt;/a&gt; and
privacy-friendly collaboration tools. It&amp;rsquo;s a complete online office
suite that is end-to-end encrypted, no one except you (and whoever you
give your access keys to) can access your data. It lets you easily
create rich text and Markdown documents, presentations, spreadsheets,
poll, forms &amp;amp; kanban in your web browser.&lt;/p&gt;
&lt;p&gt;Everyone can host their own instance following the Administrator
Guide. However, for less tech-savvy people, you can use the flagship
instance, maintained by the developers of the project. The service is
free up to a certain storage limit, with paid accounts for additional
storage. You can even use the free service without registering if you
want to. It’s worth registering though, as you can keep your documents
permanently online this way.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Project website:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://cryptpad.org&#34;&gt;cryptpad.org&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Instances:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://cryptpad.fr&#34;&gt;cryptpad.fr&lt;/a&gt; (flagship)&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://cryptpad.digitalcourage.de/&#34;&gt;cryptpad.digitalcourage.de&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Documentation:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://docs.cryptpad.org&#34;&gt;docs.cryptpad.org&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>Handwritten Notes | switching.software</title>
      <link>https://switching.software/use/handwritten-notes/</link>
      <pubDate>Wed, 15 Jul 2020 21:02:20 +0200</pubDate>
      
      <guid>https://switching.software/use/handwritten-notes/</guid>
      <description>&lt;p&gt;&lt;strong&gt;Handwritten Notes&lt;/strong&gt; offer significant advantages in a lot of use cases.
They are fast, easy and distraction-free while also never running out of battery.
Plus, you can format and arrange your notes however you like.&lt;/p&gt;
&lt;p&gt;Of course, they don&amp;rsquo;t offer backups, automatic reminders and search/share features.
But this might be fine for your short-term or spontaneous notes.
Also, it &lt;a href=&#34;https://journals.sagepub.com/doi/abs/10.1177/0956797614524581&#34;&gt;might help you remember&lt;/a&gt; your notes better.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Explore:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://doist.com/blog/pen-and-paper-productivity/&#34;&gt;Pen &amp;amp; Paper Productivity&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://www.hongkiat.com/blog/to-do-lists-by-hand/&#34;&gt;To-Do lists by hand&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>Joplin | switching.software</title>
      <link>https://switching.software/use/joplin/</link>
      <pubDate>Sun, 07 Jun 2020 13:39:43 +0200</pubDate>
      
      <guid>https://switching.software/use/joplin/</guid>
      <description>&lt;p&gt;&lt;strong&gt;Joplin&lt;/strong&gt; is a &lt;a href=&#34;https://switching.software/articles/free-libre-open-software/&#34;&gt;libre&lt;/a&gt; note taking and to-do app.
Notes are organized into notebooks and can be searched, tagged, exported and synced across devices.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Website:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://joplinapp.org/&#34;&gt;JoplinApp.org&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Download:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://joplinapp.org/#desktop-applications&#34;&gt;Windows / macOS / Linux&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://joplinapp.org/#mobile-applications&#34;&gt;Android / iOS&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>Nextcloud Notes | switching.software</title>
      <link>https://switching.software/use/nextcloud-extensions/nextcloud-notes/</link>
      <pubDate>Tue, 16 Jul 2024 13:18:09 +0000</pubDate>
      
      <guid>https://switching.software/use/nextcloud-extensions/nextcloud-notes/</guid>
      <description>&lt;p&gt;If you are using Nextcloud, the &lt;strong&gt;Notes extension&lt;/strong&gt; is a good option for organizing your notes.
You can edit them with every Nextcloud client and many third-party apps.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Website:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://apps.nextcloud.com/apps/notes&#34;&gt;Notes Extension&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Android app:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://f-droid.org/packages/it.niedermann.owncloud.notes/&#34;&gt;Nextcloud Notes (F-Droid)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://play.google.com/store/apps/details?id=it.niedermann.owncloud.notes&#34;&gt;Nextcloud Notes (Google Play Store)&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;iOS app:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://apps.apple.com/app/cloudnotes-owncloud-notes/id813973264&#34;&gt;iOS&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>Standard Notes | switching.software</title>
      <link>https://switching.software/use/standard-notes/</link>
      <pubDate>Tue, 16 Jul 2024 13:18:09 +0000</pubDate>
      
      <guid>https://switching.software/use/standard-notes/</guid>
      <description>&lt;p&gt;&lt;strong&gt;Standard Notes&lt;/strong&gt; is a simple &lt;a href=&#34;https://switching.software/articles/free-libre-open-software/&#34;&gt;open source&lt;/a&gt; notes app focussing on simplicity.
By creating an account, you can easily sync your notes across devices with end-to-end-encryption.&lt;/p&gt;
&lt;p&gt;Advanced users might be interested in the paid extended version or in hosting the sync server themselves.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Website:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://standardnotes.org/&#34;&gt;StandardNotes.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://standardnotes.org/extensions?downloaded=web&#34;&gt;Web version&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Android app:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://f-droid.org/en/packages/com.standardnotes/&#34;&gt;Standard Notes (F-Droid)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://play.google.com/store/apps/details?id=com.standardnotes&#34;&gt;Standard Notes (Google Play Store)&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;iOS app:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://apps.apple.com/us/app/standard-notes/id1285392450&#34;&gt;iOS&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Desktop app:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://standardnotes.org/#get-started&#34;&gt;Windows / macOS / Linux&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>Xournal&#43;&#43; | switching.software</title>
      <link>https://switching.software/use/xournalpp/</link>
      <pubDate>Wed, 24 Jul 2024 12:49:23 +0200</pubDate>
      
      <guid>https://switching.software/use/xournalpp/</guid>
      <description>&lt;p&gt;&lt;strong&gt;Xournal++&lt;/strong&gt; (/ˌzɚnl̟ˌplʌsˈplʌs/) is a FLOSS note-taking software for PDF
documents. It is a modern rewrite of
&lt;a href=&#34;https://sourceforge.net/projects/xournal/&#34;&gt;Xournal&lt;/a&gt; which is extremely
featureful.&lt;/p&gt;
&lt;div class=&#34;infobox bg-gray&#34;&gt;
    &lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Website:&lt;/strong&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://xournalpp.github.io/&#34;&gt;xournalpp.github.io&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://gitter.im/xournalpp/xournalpp&#34;&gt;Matrix&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://github.com/xournalpp/xournalpp/&#34;&gt;Source&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

&lt;/div&gt;
</description>
    </item>
    
  </channel>
</rss>