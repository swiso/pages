# HazzelForms
### Create, validate and mail PHP forms in minutes!

Please visit the documentation if you want to use HazzelForms:<br/>
https://hazzeldorn.github.io/HazzelForms/
<br/><br/><br/><br/>

### Changelog
===== V1.2.4 | 2020-04-07 ===== <br/>
* CSP compatibility added to HoneyPot fields
* Some code refactoring
* Translation file added: german (informal)
* Docs updated

===== V1.2.3 | 2020-03-24 ===== <br/>
Added the nl2br() function to the mail template to preserve line breaks

===== V1.2.2 | 2020-02-26 ===== <br/>
Bugfix: File upload did not accept 'required' => false

===== V1.2.1 | 2020-01-29 ===== <br/>
Bugfix: Number fields did not throw an error message when not required

===== V1.2.0 |  2020-01-13 ===== <br/>
Honeypot feature added.

===== V1.1.3 |  2019-12-19 ===== <br/>
Bugfix: File-Upload did not work when no other form fields existed.

===== V1.1.2 |  2019-12-12 ===== <br/>
Minor improvements regarding HTML structure of option fields.

===== V1.1 |  2019-12-11 ===== <br/>
First stable + tested release containing major improvements.

===== V1.0 |  2019-12-10 ===== <br/>
Initial version
